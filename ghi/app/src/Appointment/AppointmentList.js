import {useEffect, useState} from "react"

function AppointmentList() {
    const [getAppointments, setAppointment] = useState([])
    const getData = async () => {
        const appointmentUrl = 'http://localhost:8080/api/appointments/'
        const response = await fetch(appointmentUrl)
        if (response.ok) {
        const data = await response.json();
        setAppointment(data.Appointments)
        }
    }
    useEffect(() => {
        getData()
    }, [])
    const handleCancel = async (id) => {
        const cancelID = 'http://localhost:8080/api/appointments/${id}/cancel/'
        const fetchConfig = {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(cancelID, fetchConfig)
        if (response.ok) {
            alert('Appointment Cancelled Succesfully')
            console.log('This appointment has been canceled')
            const urlReload = 'http://localhost:8080/api/appointments/'
        fetch(urlReload)
        } else {
            alert('Error Canceling Appointment')
            console.log('Could not cancel this appointment')
        }
    }
    const handleFinish = async (id) => {
        const finishID = 'http://localhost:8080/api/appointments/${id}/finish/'
        const fetchConfig = {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(finishID, fetchConfig)
        if (response.ok) {
            alert('Appointment Completed Succesfully')
            console.log('Appointment was finished Succesfully')
            const urlReload = 'http://localhost:8080/api/appointments/'
        fetch(urlReload)
        } else {
            alert('Could Not Finish Appointment')
            console.error('error marking finished appointment')
        }
    }
        return (
        <div>
            <h1>Service Appointments</h1>
            <table className="table table-striped">
                <thead>
                <tr>
                    <th>VIN</th>
                    <th>Is VIP?</th>
                    <th>Customer</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Technician</th>
                    <th>Reason</th>
                </tr>
                </thead>
                <tbody>
                {getAppointments.map((Appointments) => {
                    return (
                    <tr key={Appointments.id}>
                        <td>{Appointments.vin}</td>
                        <td>{Appointments.is_vip ? "Yes" : " No"}</td>
                        <td>{Appointments.customer}</td>
                        <td>{new Date(Appointments.date_time).toLocaleDateString("en-US")}</td>
                        <td>{new Date(Appointments.date_time).toLocaleTimeString("en-US")}</td>
                        <td>{Appointments.technician.first_name} {Appointments.technician.last_name}</td>
                        <td>{Appointments.reason}</td>
                        <td><button onClick={() => handleCancel(Appointments.id)}>Cancel</button></td>
                        <td><button onClick={() => handleFinish(Appointments.id)}>Finish</button></td>
                    </tr>
                    );
                })}
                </tbody>
            </table>
        </div>
        );
    }
    export default AppointmentList;
