import {useEffect, useState} from "react"

function AutomobileList() {
    const [automobile, setAutomobiles] = useState([])
    const getAutomobileData = async () => {
    const automobilesUrl = 'http://localhost:8100/api/automobiles/'
    const response = await fetch(automobilesUrl)
    if (response.ok) {
        const data = await response.json();
        setAutomobiles(data.autos)
    }
    }
    useEffect(() => {
        getAutomobileData()
    }, [])
    return (
    <div>
        <h1>Automobiles</h1>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Vin</th>
                    <th>Color</th>
                    <th>Year</th>
                    <th>Model</th>
                    <th>Manufacturer</th>
                    <th>Sold</th>
                </tr>
            </thead>
            <tbody>
            {automobile.map(autos => {
                return (
                    <tr key={autos.id}>
                        <td>{autos.vin}</td>
                        <td>{autos.color}</td>
                        <td>{autos.year}</td>
                        <td>{autos.model.name}</td>
                        <td>{autos.model.manufacturer.name}</td>
                        <td>{autos.sold ? "Yes" : " No"}</td>
                    </tr>
                    );
            })}
            </tbody>
        </table>
    </div>
    )
}
export default AutomobileList
