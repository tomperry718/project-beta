import { useState } from 'react'

function CustomerForm() {
    const [formData, setFormData] = useState({
        first_name: '',
        last_name: '',
        address: '',
        phone_number: '',
    })
    const handleSubmit = async (event) => {
        event.preventDefault()
        const customersurl = 'http://localhost:8090/api/customers/'
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(formData),
            headers: {
                "Content-Type": 'application/json',
            },
        }
        const response = await fetch(customersurl, fetchConfig)
        if (response.ok) {
            setFormData({
                first_name: '',
                last_name: '',
                address: '',
                phone_number: '',
            })
        }
    }
    const handleFormChange = (e) => {
        const value = e.target.value
        const inputCustomer = e.target.name
        setFormData({
            ...formData,
            [inputCustomer]: value,
        })
    }
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Customer</h1>
                        <form onSubmit={handleSubmit} id="create-customer-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} placeholder="First Name" required type="text" name="first_name" id="first_name" className="form-control" value={formData.first_name}/>
                            <label htmlFor="first_name">First name...</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} placeholder="Last Name" required type="text" name="last_name" id="last_name" className="form-control" value={formData.last_name}/>
                            <label htmlFor="last_name">Last name...</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} placeholder="address" required type="text" name="address" id="address" className="form-control" value={formData.address}/>
                            <label htmlFor="address">Address...</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} placeholder="phone_number" required type="text" name="phone_number" id="phone_number" className="form-control" value={formData.phone_number}/>
                            <label htmlFor="phone_number">Phone Number...</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}
export default CustomerForm;
