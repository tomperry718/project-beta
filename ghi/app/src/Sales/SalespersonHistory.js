import { useEffect, useState } from 'react'

function SalespersonHistoryForm() {
    const [formData, setFormData] = useState({
        salesperson: '',
    })
    const [getSalespeople, setSalespeople] = useState([])
    const [salesHistory, setSalesHistory] = useState([]);
    const getSalespeopleData = async () => {
        const salespeopleUrl = 'http://localhost:8090/api/salespeople/'
        const response = await fetch(salespeopleUrl)
        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespersons)
            }
        }
    const fetchSalesHistory = async () => {
        const salesHistoryUrl = `http://localhost:8090/api/salespeople/`;
        const response = await fetch(salesHistoryUrl);
        if (response.ok) {
            const data = await response.json();
            setSalesHistory(data.sales);
        }
    };
        useEffect(() => {
            getSalespeopleData()
        }, []);
    const handleFormChange = (e) => {
        const value = e.target.value
        const inputSale = e.target.name
        setFormData({
            ...formData,
            [inputSale]: value,
        })
    }
    const handleSubmit = (e) => {
        e.preventDefault();
        const { salesperson } = formData;
        fetchSalesHistory(salesperson);
    };
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Salesperson History</h1>
                        <form onSubmit={handleSubmit} id="salesperson-history-form">
                        <div className="mb-3">
                            <select onChange={handleFormChange} required name="salesperson" id="salesperson" className="form-select">
                            <option value="">Choose a Salesperson</option>
                            {getSalespeople.map((salesperson) => (
                            <option key={salesperson.id} value={salesperson.id}>
                            {salesperson.first_name} {salesperson.last_name}
                            </option>
                            ))}
                            </select>
                        </div>
                <button className="btn btn-primary">Show Sales History</button>
                </form>
                {salesHistory.length > 0 && (
                    <div>
                        <h2>Sales History</h2>
                            <table className="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Salesperson</th>
                                        <th>Customer</th>
                                        <th>Automobile VIN</th>
                                        <th>Price</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {salesHistory.map((sale) => (
                                    <tr key={sale.id}>
                                        <td>{sale.salesperson_first} {sale.salesperson_last}</td>
                                        <td>{sale.customer_first} {sale.customer_last}</td>
                                        <td>{sale.vin}</td>
                                        <td>{sale.price}</td>
                                        </tr>
                                        ))}
                                </tbody>
                            </table>
                    </div>
                    )}
                </div>
            </div>
        </div>
        );
    }
export default SalespersonHistoryForm;
