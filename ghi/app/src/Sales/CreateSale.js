import { useEffect, useState } from 'react'

function SaleForm() {
    const [formData, setFormData] = useState({
        automobile: '',
        salesperson: '',
        customer: '',
        price: '',
    })
    const [getAutomobiles, setAutomobiles] = useState([])
    const [getSalespeople, setSalespeople] = useState([])
    const [getCustomers, setCustomers] = useState([])
    const selectedAutomobileVin = formData.automobile
    const handleSubmit = async (event) => {
        event.preventDefault()
        const saleurl = 'http://localhost:8090/api/sales/'
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(formData),
            headers: {
                "Content-Type": 'application/json',
            },
        }
        const response = await fetch(saleurl, fetchConfig)
        if (response.ok) {
            setFormData({
                automobile: '',
                salesperson: '',
                customer: '',
                price: '',
            })
        }
        const currentCar = getAutomobiles?.filter(automobile => automobile.vin === selectedAutomobileVin)
        const inventoryurl = `http://localhost:8100/api/automobiles/${selectedAutomobileVin}/`
        const inventoryFetchConfig = {
            method: 'PUT',
            body: JSON.stringify({
                color: currentCar.color,
                year: currentCar.year,
                sold: true
            }),
            headers: {
                "Content-Type": 'application/json',
            },
        }
        const inventoryResponse = await fetch(inventoryurl, inventoryFetchConfig)
    }
    const handleFormChange = (e) => {
        const value = e.target.value
        const inputSale = e.target.name
        setFormData({
            ...formData,
            [inputSale]: value,
        })
    }
    const getAutombilesData = async () => {
        const automobilesUrl = 'http://localhost:8100/api/automobiles/'
        const response = await fetch(automobilesUrl)
        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.autos)
            }
        }
    const getSalespeopleData = async () => {
        const salespeopleUrl = 'http://localhost:8090/api/salespeople/'
        const response = await fetch(salespeopleUrl)
        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespersons)
            }
        }
    const getCustomersData = async () => {
        const customersUrl = 'http://localhost:8090/api/customers/'
        const response = await fetch(customersUrl)
        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers)
            }
        }
    useEffect(() => {
        getAutombilesData()
        getSalespeopleData()
        getCustomersData()
    }, []);
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a Sale</h1>
                        <form onSubmit={handleSubmit} id="create-sale-form">
                        <div className="mb-3">
                            <select onChange={handleFormChange} required name="automobile" id="automobile" className="form-select" value={formData.automobile}>
                            <option value="">Choose a Automobile</option>
                            {getAutomobiles.filter(automobile => !automobile.sold).map(automobile => (
                            <option key={automobile.vin} value={automobile.vin}>{automobile.vin}</option>
                            ))}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleFormChange} required name="salesperson" id="salesperson" className="form-select" value={formData.salesperson}>
                            <option value="">Choose a Salesperson</option>
                            {getSalespeople.map(salespersons => (
                            <option key={salespersons.id} value={salespersons.id}>{salespersons.first_name} {salespersons.last_name}</option>
                            ))}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleFormChange} required name="customer" id="customer" className="form-select" value={formData.customer}>
                            <option value="">Choose a Customer</option>
                            {getCustomers.map(customers => (
                            <option key={customers.id} value={customers.id}>{customers.first_name} {customers.last_name}</option>
                            ))}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={formData.price} onChange={handleFormChange} placeholder="price" required type="text" name="price" id="price" className="form-control"/>
                            <label htmlFor="price">Price...</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}
export default SaleForm;
