import {useEffect, useState} from "react"

function SalesPeopleList() {
    const [salespeople, setSalespeople] = useState([])
    const getData = async () => {
    const salespeopleUrl = 'http://localhost:8090/api/salespeople/'
    const response = await fetch(salespeopleUrl)
    if (response.ok) {
        const data = await response.json();
        setSalespeople(data.salespersons)
    }
    }
    useEffect(() => {
        getData()
    }, [])
    return (
    <div>
        <h1>Salespeople</h1>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Employee ID</th>
                </tr>
            </thead>
            <tbody>
            {salespeople.map(salespersons => {
                return (
                    <tr key={salespersons.id}>
                        <td>{salespersons.first_name}</td>
                        <td>{salespersons.last_name}</td>
                        <td>{salespersons.employee_id}</td>
                    </tr>
                    );
            })}
            </tbody>
        </table>
    </div>
    );
}
export default SalesPeopleList;
