import { useEffect, useState } from 'react'

function CreateModelForm() {
    const [formData, setFormData] = useState({
        name: '',
        manufacturer_id: '',
        picture_url: '',
    })
    const [getManufacturers, setManufacturers] = useState([])
    const handleSubmit = async (event) => {
        event.preventDefault()
        const modelsUrl = 'http://localhost:8100/api/models/'
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(formData),
            headers: {
                "Content-Type": 'application/json',
            },
        }
        const response = await fetch(modelsUrl, fetchConfig)
        if (response.ok) {
            setFormData({
                name: '',
                manufacturer_id: '',
                picture_url: '',
            })
        }
    }
    const handleFormChange = (e) => {
        const value = e.target.value
        const inputModel = e.target.name
        setFormData({
            ...formData,
            [inputModel]: value,
        })
    }
    useEffect(() => {
        const getData = async () => {
            const manufacturersUrl = 'http://localhost:8100/api/manufacturers/'
            const response = await fetch(manufacturersUrl)
            if (response.ok) {
                const data = await response.json()
                setManufacturers(data.manufacturers)
            }
        }
        getData()
    }, [])
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a Model</h1>
                        <form onSubmit={handleSubmit} id="create-model-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.name} placeholder="name" required type="text" name="name" id="name" className="form-control" />
                            <label htmlFor="name">Model Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.picture_url} placeholder="Enter URL" required type="text" name="picture_url" id="picture_url" className="form-control" />
                            <label htmlFor="picture_url">Picture URL</label>
                        </div>
                        <div className="form-floating mb-3">
                            <select onChange={handleFormChange} value={formData.manufacturer_id} name="manufacturer_id" id="manufacturer_id" className="form-select">
                                <option value="">Choose a Manufacturer</option>
                                {getManufacturers.map((manufacturers) => (
                                    <option key={manufacturers.id} value={manufacturers.id}> 
                                        {manufacturers.name}
                                    </option>  
                                ))}
                            </select>
                            <label htmlFor="manufacturer">Manufacturer</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
        )
    }
    export default CreateModelForm
