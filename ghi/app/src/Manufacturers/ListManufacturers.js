import {useEffect, useState} from "react"

function ManufacturerList() {
    const [manufacturer, setManufacturers] = useState([])
    const getData = async () => {
    const manufacturersUrl = 'http://localhost:8100/api/manufacturers/'
    const response = await fetch(manufacturersUrl)
    if (response.ok) {
        const data = await response.json();
        setManufacturers(data.manufacturers)
        }
    }
    useEffect(() => {
        getData()
    },[])
        return (
        <div>
            <h1>Manufacturers</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                {manufacturer.map(manufacturer => {
                    return (
                        <tr key={manufacturer.id}>
                            <td>{manufacturer.name}</td>
                        </tr>
                        );
                })}
                </tbody>
            </table>
        </div>
        )
    }
    export default ManufacturerList
