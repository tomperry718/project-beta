import {useEffect, useState} from "react"

function TechnicianList() {
    const [getTechnicians, setTechnician] = useState([])
    const getData = async () => {
        const technicianUrl = 'http://localhost:8080/api/technicians/'
        const response = await fetch(technicianUrl)
        
        if (response.ok) {
        const data = await response.json();
        setTechnician(data.Technicians)
        }
    }
    useEffect(() => {
        getData()
    }, [])
        return (
        <div>
            <h1>Technicians</h1>
            <table className="table table-striped">
                <thead>
                <tr>
                    <th>Employee ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                </tr>
                </thead>
                <tbody>
                {getTechnicians.map(Technicians => {
                    return (
                    <tr key={Technicians.id}>
                        <td>{Technicians.employee_id}</td>
                        <td>{Technicians.first_name}</td>
                        <td>{Technicians.last_name}</td>
                    </tr>
                    );
                })}
                </tbody>
            </table>
        </div>
        );
    }
    export default TechnicianList;
