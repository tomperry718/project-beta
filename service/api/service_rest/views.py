from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .encoders import (
    TechnicianListEncoder,
    AppointmentListEncoder,
)
from .models import Technician, Appointment


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"Technicians": technicians},
            encoder=TechnicianListEncoder,
            safe=False,
        )
    else:
        try:
            content = json.loads(request.body)
            new_technician = Technician.objects.create(**content)
            return JsonResponse(
                new_technician,
                encoder=TechnicianListEncoder,
                safe=False,
            )
        except:
            return JsonResponse(
                {"message": "Could not add a new technician"},
                status=400
            )


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"Appointments": appointments},
            encoder=AppointmentListEncoder,
            safe=False,
        )
    else:
        try:
            content = json.loads(request.body)
            content['date_time'] = f"{content['date']} {content['time']}"
            del content['date']
            del content['time']
            techID = content['technician']
            technician = Technician.objects.get(employee_id=techID)
            content['technician'] = technician
            new_appointment = Appointment.objects.create(**content)
            return JsonResponse(
                new_appointment,
                encoder=AppointmentListEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Could not add a new appointment"},
                status=400
            )


@require_http_methods(["DELETE", "GET"])
def api_show_technician(request, pk):
    if request.method == "DELETE":
        count, _ = Technician.objects.filter(id=pk).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )
    else:
        try:
            technician = Technician.objects.get(id=pk)
            return JsonResponse(
                technician,
                encoder=TechnicianListEncoder,
                safe=False
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "This technician does not exist in the database"},
                status=400
            )


@require_http_methods(["DELETE", "GET"])
def api_show_appointment(request, pk):
    if request.method == "DELETE":
        count, _ = Appointment.objects.filter(id=pk).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )
    else:
        try:
            appointment = Appointment.objects.get(id=pk)
            return JsonResponse(
                appointment,
                encoder=AppointmentListEncoder,
                safe=False
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "This appointment does not exist in the database"},
                status=400
            )


@require_http_methods(["PUT"])
def api_cancel_appointment(request, pk):
    if request.method == "PUT":
        try:
            cancel_appointment = Appointment.objects.get(id=pk)
            if cancel_appointment.status == "CANCELED":
                return JsonResponse(
                    {"message": "Appointment is already canceled"},
                    status=400
                )
            elif cancel_appointment.status == "FINISHED":
                return JsonResponse(
                    {"message": "Appointment is already finished"},
                    status=400
                )
            cancel_appointment.status = "CANCELED"
            cancel_appointment.save()
            return JsonResponse(
                cancel_appointment,
                encoder=AppointmentListEncoder,
                safe=False
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Cannot be cancel a non-existing appointment"},
                status=400
            )


@require_http_methods(["PUT"])
def api_finished_appointment(request, pk):
    if request.method == "PUT":
        try:
            finish_appointment = Appointment.objects.get(id=pk)
            if finish_appointment.status == "FINISHED":
                return JsonResponse(
                    {"message": "Appointment is already marked finished"},
                    status=400
                )
            elif finish_appointment.status == "CANCELED":
                return JsonResponse(
                    {"messge": "This service request has been canceled"}
                )
            finish_appointment.status = "FINISHED"
            finish_appointment.save()
            return JsonResponse(
                finish_appointment,
                encoder=AppointmentListEncoder,
                safe=False
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Cannot be complete a non-existing appointment"},
                status=400
            )
