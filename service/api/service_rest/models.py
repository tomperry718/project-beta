from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)


class Technician(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    employee_id = models.CharField(max_length=20, unique=True)


class Appointment(models.Model):
    date_time = models.DateTimeField(auto_now=False, auto_now_add=False)
    reason = models.CharField(max_length=100)
    vin = models.CharField(max_length=17, unique=True)
    customer = models.CharField(max_length=100)
    status = models.CharField(max_length=10, default="CREATED")
    technician = models.ForeignKey(
        Technician,
        related_name="appointment",
        on_delete=models.PROTECT
    )
    is_vip = models.BooleanField(default=False)

    def created(self):
        self.status = "CREATED"
        self.save()

    def finished(self):
        self.status = 'FINISHED'
        self.save()

    def canceled(self):
        self.status = "CANCELED"
        self.save()

    def vip_status(self):
        vin_exist = AutomobileVO.objects.filter(vin=self.vin, sold=True)
        if vin_exist.exists():
            return True
        else:
            return False

    def save_vip(self, *args, **kwargs):
        self.is_vip = self.vip_status()
        super().save(*args, **kwargs)
