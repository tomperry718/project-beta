# CarCar

Team:

* Tom - Sales
* Tamekia - Services

## Design

## Service microservice

The service models creates a way for the end user to input a
service request appointment and add a technician to the system.
It also allows for a user to list the appointments and view various
sets of data and view a list of technicians. User are able to access
the data to set appointments to canceled or finished as well as
delete certain appointments and technicians.

This intergrates with the inventory microservice by showing customers
that are vip members and together it allows for a full automotive tracking
system that can cars inventory, customer appointment and sales.

## Sales microservice

Explain your models and integration with the inventory
microservice, here.

I have four models in my microservice: Salesperson, Customer, AutomobileVO, and Sale. The create sale feature in my microservice has the quiet a bit of integration with the inventory microservice. I use the AutomobileVO to ensure vehicles I sell are instances of automobiles generated and pulled over from inventory through my poller.py. Then upon successful creation of a sale my code runs a "PUT" request back to inventory to ensure that it tracks an automobile in their database reflects sold.
